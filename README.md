# SpringBoot API - User Registration

## Step 1 - Run SpringBoot API 

Open a terminal from the project folder. 

Execute the command `./mvnw clean install` then `./mvnw spring-boot:run`.

## Step 2 - Connect to H2 Database

Open a browser and go to <http://localhost:8080/h2/>.

At the login page, you need to provide connection information to connect to the database : 

`Saved Settings : Generic H2 (Embedded)`\
`Driver Class : org.h2.Driver`\
`JDBC URL : jdbc:h2:./testdb`\
`User Name : root`\
`Password : root`

Then, click on `Connect`.

In the query panel, type the following SQL command : `SELECT * FROM USER` and then, click on `Run`. 

The table appears just below the command. 

## Step 3 - Send requests with Postman

Open Postman and import the provided collection **SpringBoot_API.postman_collection.json** to have request samples.

## Setup Lombok with Eclipse (if needed)

If you want to open and run the project in Eclipse, you need to setup **Lombok**. 

Get the latest version of Lombok <https://projectlombok.org/download>.

Double click on downloaded file. 

Specify the location of your IDE eclipse.exe, click on **Install/Update** button. 

If the installation is successful, you can exit the installer. Don't forget to restart the IDE and ensure that Lombok is correctly configured. 

You can check this in Eclipse (`Help > About Eclipse IDE`). 

Then, run **SpringBootApiApplication.java** as **Spring Boot App**.

For tests, right click **src/test/java** and run as **JUnit Test**.