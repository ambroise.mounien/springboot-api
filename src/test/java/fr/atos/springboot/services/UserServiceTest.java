package fr.atos.springboot.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.server.ResponseStatusException;

import fr.atos.springboot.ServiceTestBase;
import fr.atos.springboot.models.Gender;
import fr.atos.springboot.models.User;

@SpringBootTest
public class UserServiceTest extends ServiceTestBase{
	
	@Autowired
	private UserService us;
	
	@AfterEach
	public void cleanUp() {
		reset(userRepository, pe);
	}
	
	@Test
	void findByIdSuccess() {
		User u = User.builder().id(123).build();
		when(userRepository.findById(123L)).thenReturn(Optional.of(u));
		
		Optional<User> result = us.findById(123);
		
		assertEquals(u, result.get());
		verify(userRepository, times(1)).findById(123L);
	}
	
	@Test
	public void findByIdFail() throws Exception {

		when(userRepository.findById(123L)).thenReturn(Optional.empty());
		doThrow(ResponseStatusException.class).when(userRepository).findById(123L);

		assertThrows(ResponseStatusException.class, () -> us.findById(123));
		verify(userRepository, only()).findById(123L);
	}
	
	@Test
	void findAllSuccess() {
		// arrange
		List<User> users = List.of(
				User.builder().id(1).build(),
				User.builder().id(2).build(),
				User.builder().id(3).build()
				);
		when(userRepository.findAll()).thenReturn(users);
		// act
		Collection<User> result = us.findAll();
		// assert
		assertEquals(users, result);
		verify(userRepository, only()).findAll();
	}
	
	@Test
	public void saveSuccess() {
		LocalDate birthdate = LocalDate.parse("05-12-1997",DateTimeFormatter.ofPattern("dd-MM-yyyy"));

		User u = User.builder()
			     .username("user1")
			     .birthdate(birthdate)
			     .country("France")
			     .phoneNumber("0123456789")
			     .gender(Gender.MALE)
			     .password("pwd")
			     .build();
		
		us.save(u);
		
		verify(userRepository, times(1)).save(u);
		verify(userRepository, times(1)).findByUsername(u.getUsername());
		verify(userRepository, times(1)).findByPhoneNumber(u.getPhoneNumber());
	}
	
	@Test
	public void saveFail() {
		LocalDate birthdate = LocalDate.parse("05-12-1997",DateTimeFormatter.ofPattern("dd-MM-yyyy"));

		User u = User.builder()
			     .username("user1")
			     .birthdate(birthdate)
			     .country("France")
			     .phoneNumber("0123456789")
			     .gender(Gender.MALE)
			     .password("pwd")
			     .build();
		
		doThrow(ResponseStatusException.class).when(userRepository).save(u);

		assertThrows(ResponseStatusException.class, () -> us.save(u));
		verify(userRepository, times(1)).save(u);
		verify(userRepository, times(1)).findByUsername(u.getUsername());
		verify(userRepository, times(1)).findByPhoneNumber(u.getPhoneNumber());
	}
}
