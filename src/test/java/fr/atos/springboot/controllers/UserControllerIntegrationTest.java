package fr.atos.springboot.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.atos.springboot.ControllerIntegrationTestBase;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
public class UserControllerIntegrationTest extends ControllerIntegrationTestBase{

	@Autowired
	private MockMvc mvc;
	
	private ObjectMapper json = new ObjectMapper();
	
	@AfterEach
	public void cleanUp() {
		reset(userService); 
	}
	
	@Test
	@Order(1)
	public void findAllSuccess() throws Exception {
		var result = mvc.perform(get("/user"));

		result.andExpect(status().isOk())
			  .andExpect(jsonPath("[0].username", is("ambroise")))
			  .andExpect(jsonPath("[1].username", is("user2")))
		      .andDo(print());
		verify(userService, only()).findAll();
	}
	
	@Test
	@Order(2)
	public void findByIdSuccess() throws Exception {
		
		var result = mvc.perform(get("/user/id/1001"));
		
		result.andExpect(status().isOk())
		      .andExpect(jsonPath("id", is(1001)))
		      .andExpect(jsonPath("username", is("ambroise")));
		verify(userService, only()).findById(1001);
	}
	
	@Test
	@Order(3)
	public void findByIdFail() throws Exception {
		
		var result = mvc.perform(get("/user/id/3"));
		
		result.andExpect(status().isNotFound());
		verify(userService, only()).findById(3);
	}
	
	@Test
	@Order(4)
	public void saveSuccess() throws Exception {
		var result = mvc.perform(post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.writeValueAsBytes(customUser))
               );

		result.andExpect(status().isOk());
		result = mvc.perform(get("/user/id/1"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("username", is("user5")));
	}
	
	@Test
	@Order(5)
	public void usernameConflict() throws Exception {
		
		var result = mvc.perform(post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.writeValueAsBytes(customUser))
               );

		result.andExpect(status().isConflict());
		result = mvc.perform(get("/user/id/2"))
				.andExpect(status().isNotFound());
	}
	
	@Test
	@Order(6)
	public void phoneNumberConflict() throws Exception {
		customUser.setUsername("user6");
		var result = mvc.perform(post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.writeValueAsBytes(customUser))
               );

		result.andExpect(status().isConflict());
		result = mvc.perform(get("/user/id/2"))
				.andExpect(status().isNotFound());
	}
	
	@Test
	@Order(7)
	public void invalidCountry() throws Exception {
		customUser.setUsername("user6");
		customUser.setPhoneNumber("0202020202");
		customUser.setCountry("USA");
		var result = mvc.perform(post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.writeValueAsBytes(customUser))
               );

		result.andExpect(status().isNotAcceptable());
	}
	
	@Test
	@Order(8)
	public void invalidAge() throws Exception {
		customUser.setUsername("user6");
		customUser.setPhoneNumber("0202020202");
		customUser.setCountry("France");
		customUser.setBirthdate(LocalDate.now());
		var result = mvc.perform(post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.writeValueAsBytes(customUser))
               );

		result.andExpect(status().isNotAcceptable());
	
		// Test for a date in future
		customUser.setBirthdate(LocalDate.now().plusMonths(2));
		result = mvc.perform(post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.writeValueAsBytes(customUser))
               );

		result.andExpect(status().isNotAcceptable());
		
	}
}
