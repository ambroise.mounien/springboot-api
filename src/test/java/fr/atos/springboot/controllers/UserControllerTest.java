package fr.atos.springboot.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.junit.Before;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.atos.springboot.ControllerTestBase;
import fr.atos.springboot.models.Gender;
import fr.atos.springboot.models.User;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerTest extends ControllerTestBase{
	
	@Autowired
	private MockMvc mvc;
	
	private ObjectMapper json = new ObjectMapper();
	
	@Autowired
    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }
    
	@AfterEach
	public void cleanUp() {
		reset(userService);
	}
	
	@Test
	public void findAllSuccess() throws Exception {

		Collection<User> users = List.of(
				User.builder().id(1).build(),
				User.builder().id(2).build(),
				User.builder().id(3).build()
				);
		when(userService.findAll()).thenReturn(users);

		var result = mvc.perform(get("/user"));

		result.andExpect(status().isOk())
			  .andExpect(content().contentType(MediaType.APPLICATION_JSON))
			  .andExpect(jsonPath("[0].id", is(1)))
			  .andExpect(jsonPath("[1].id", is(2)))
			  .andExpect(jsonPath("[2].id", is(3)));
		verify(userService, only()).findAll();
	}
	
	@Test
	public void findByIdSuccess() throws Exception {
		User u = User.builder().id(123).build();
		
		when(userService.findById(123L)).thenReturn(Optional.of(u));
		
		var result = mvc.perform(get("/user/id/123"));
		
		result.andExpect(status().isOk());
		verify(userService, only()).findById(123L);
	}
	
	@Test
	public void findByIdFail() throws Exception {
		when(userService.findById(123L)).thenReturn(Optional.empty());
		
		var result = mvc.perform(get("/user/id/123"));
		
		result.andExpect(status().isNotFound());
		verify(userService, only()).findById(123L);
	}
	
	@Test
	public void findByUsernameSuccess() throws Exception {
		User u = User.builder().username("user123").build();
		
		when(userService.findByUsername("user123")).thenReturn(Optional.of(u));
		
		var result = mvc.perform(get("/user/username/user123"));
		
		result.andExpect(status().isOk());
		verify(userService, only()).findByUsername("user123");
	}
	
	@Test
	public void findByUsernameFail() throws Exception {
		when(userService.findByUsername("user123")).thenReturn(Optional.empty());
		
		var result = mvc.perform(get("/user/username/user123"));
		
		result.andExpect(status().isNotFound());
		verify(userService, only()).findByUsername("user123");
	}
	
	@Test
	public void saveSuccess() throws Exception {
		LocalDate birthdate = LocalDate.parse("05-12-1997",DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		
		User u = User.builder()
				     .username("user1")
				     .birthdate(birthdate)
				     .country("France")
				     .phoneNumber("0123456789")
				     .gender(Gender.MALE)
				     .password("pwd")
				     .build();

		var result = mvc.perform(post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.writeValueAsBytes(u))
               );

		result.andExpect(status().isOk());
		verify(userService, only()).save(u);
	}
	
	@Test
	public void testSaveValidationBlank() throws Exception {
		
		User u = new User();
		
		var result = mvc.perform(post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.writeValueAsBytes(u))
               );
		
		Set<ConstraintViolation<User>> violations = validator.validate(u);
		assertThat(violations.size()).isEqualTo(4);
		result.andExpect(status().isBadRequest())
		      .andDo(print()); 
		
	}
	
	@Test
	public void testSaveValidationPattern() throws Exception {
		
		LocalDate birthdate = LocalDate.parse("05-12-1997",DateTimeFormatter.ofPattern("dd-MM-yyyy"));
		
		User u = User.builder()
				     .username("user1")
				     .birthdate(birthdate)
				     .country("123") //[Error] The country of residence must contain only letters
				     .phoneNumber("012345678a") //[Error] The phone number must contain 10 numbers only
				     .password("pwd")
				     .build();
		
		var result = mvc.perform(post("/user/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json.writeValueAsBytes(u))
               ); 
		
		Set<ConstraintViolation<User>> violations = validator.validate(u);
		assertThat(violations.size()).isEqualTo(2);
		result.andExpect(status().isBadRequest())
		      .andDo(print()); 
	}
	
}
