package fr.atos.springboot;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;

import fr.atos.springboot.models.Gender;
import fr.atos.springboot.models.User;
import fr.atos.springboot.services.UserService;


@SpringBootTest
@ActiveProfiles("integration-test")
public class ControllerIntegrationTestBase {
	protected LocalDate birthdate = LocalDate.parse("05-12-1997",DateTimeFormatter.ofPattern("dd-MM-yyyy"));
	protected User customUser = User.builder()
		     .username("user5")
		     .birthdate(birthdate)
		     .country("France")
		     .phoneNumber("0101010101")
		     .gender(Gender.MALE)
		     .password("pwd3")
		     .build();
	
	@SpyBean
	protected UserService userService;

}
