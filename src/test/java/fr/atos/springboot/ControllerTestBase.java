package fr.atos.springboot;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import fr.atos.springboot.services.UserService;

@SpringBootTest
@ActiveProfiles("unit-test")
public class ControllerTestBase {

	@MockBean
	protected UserService userService;
	
}
