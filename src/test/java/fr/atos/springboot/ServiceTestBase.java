package fr.atos.springboot;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;

import fr.atos.springboot.repositories.UserRepository;

@SpringBootTest
public class ServiceTestBase {
	
	@MockBean
	protected UserRepository userRepository;
	
	@MockBean
	protected PasswordEncoder pe;
}
