package fr.atos.springboot.controllers;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import fr.atos.springboot.models.User;
import fr.atos.springboot.services.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService us;
	
	@PostMapping("/register")
	public ResponseEntity<String> save(@Valid @RequestBody User u, Errors errors) {
		if(errors.hasErrors()) {
			String msg = "";
			for(var err : errors.getAllErrors()) {
				msg += "[Error] " + err.getDefaultMessage() + "\n";
			}
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(msg);
		}
		return us.save(u);
	}
	
	@GetMapping("/id/{id}")
	public User findById(@PathVariable long id) {
		return us.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,"User with id " + id + " does not exist"));
	}
	
	@GetMapping("/username/{username}")
	public User findByUsername(@PathVariable String username) {
		return us.findByUsername(username).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND,"User with username '" + username + "' does not exist"));
	}
	
	@GetMapping("")
	public Collection<User> findAll() {
		return us.findAll();
	}
	
}
