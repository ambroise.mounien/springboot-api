package fr.atos.springboot.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;

@RestControllerAdvice
public class ExceptionHandlingController {
	
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<String> invalidFormatException(HttpMessageNotReadableException e) {
    	String msg="", fieldName="";
        if ( e.getCause() instanceof JsonParseException) {
            JsonParseException jpe = (JsonParseException)  e.getCause();
            msg = jpe.getOriginalMessage();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(msg);
        }else if (e.getCause() instanceof JsonMappingException) {
            JsonMappingException jme = (JsonMappingException) e.getCause();
	        if (jme.getPath() != null && jme.getPath().size()>0) 
	        	fieldName=jme.getPath().get(0).getFieldName();
        }else {
	    	MismatchedInputException mie = (MismatchedInputException) e.getCause();
	        if (mie.getPath() != null && mie.getPath().size()>0) 
	        	fieldName=mie.getPath().get(0).getFieldName();
        }
        msg += "Invalid input data: " + fieldName;
        if(fieldName.equals("birthdate")) {
        	msg += " (allowed format : 'dd-MM-yyyy')";
        }else if(fieldName.equals("gender")) {
        	msg += " (allowed values : 'MALE','FEMALE')";
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(msg);
    }
    
}