package fr.atos.springboot.services;

import java.time.LocalDate;
import java.time.Period;
import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import fr.atos.springboot.models.User;
import fr.atos.springboot.repositories.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository ur;
	
	@Autowired
	private PasswordEncoder pe;
	
	public ResponseEntity<String> save(User u) {
		String msg="[Error] ";
		u.setPassword(pe.encode(u.getPassword()));
		if(u.getPhoneNumber().isEmpty())
			u.setPhoneNumber(null);
		
		if(!this.findByUsername(u.getUsername()).isEmpty())
			return ResponseEntity.status(HttpStatus.CONFLICT).body(msg + "This username already exists !");
		else if(u.getPhoneNumber() != null &&  !ur.findByPhoneNumber(u.getPhoneNumber()).isEmpty())
			return ResponseEntity.status(HttpStatus.CONFLICT).body(msg + "This phone number already exists !");
		else if(LocalDate.now().isBefore(u.getBirthdate()))
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(msg + "The birthdate must be in the past !");
		else if(!u.getCountry().toLowerCase().equals("france") || (Period.between(u.getBirthdate(), LocalDate.now()).getYears() < 18)) {
			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(msg + "Only adult French residents are allowed to create account !");
		}
		
			ur.save(u);
		return ResponseEntity.status(HttpStatus.OK).body(null);
	}

	public Optional<User> findById(long id) {
		return ur.findById(id);
	}
	
	public Optional<User> findByUsername(String username){
		return ur.findByUsername(username);
	}

	public Collection<User> findAll() {
		return ur.findAll();
	}
}
