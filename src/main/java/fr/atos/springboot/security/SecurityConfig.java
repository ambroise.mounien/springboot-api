package fr.atos.springboot.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			// deactivate session
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
			// no login
			.formLogin().disable()
			// no logout
			.logout().disable()
			// no csrf
			.csrf().disable()
			.headers().frameOptions().disable().and()
			// url access rules
			.authorizeRequests()
			.antMatchers("/**").permitAll().and()
			.httpBasic();
	}

}