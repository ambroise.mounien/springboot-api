package fr.atos.springboot.models;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Entity
public class User {
	@EqualsAndHashCode.Include
	@Id
	@GeneratedValue
	protected long id;
	
	@Column(unique = true)
	@NotBlank(message="The username is required")
	private String username; 
	
	@JsonFormat(pattern="dd-MM-yyyy")
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonDeserialize(using = LocalDateDeserializer.class)
	@NotNull(message="The birthdate is required")
	private LocalDate birthdate;
	
	@NotBlank(message="The country is required")
	@Pattern(regexp="^[a-zA-Z]+$", message = "The country of residence must contain only letters")
	private String country;
	
	@Column(unique = true)
	@Pattern(regexp="^([0-9]{10})?$", message = "The phone number must contain 10 numbers only")
	private String phoneNumber;
	
	private Gender gender;
	
	@NotBlank(message="The password is required")
	private String password;
	
	public User() {
		super();
	}
}


