package fr.atos.springboot.models;

public enum Gender {
	MALE,
	FEMALE
}
