package fr.atos.springboot.aspects;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LogAspect {
	
	@Around("execution(* fr.atos.springboot.services.*.*(..))")
    public Object logAround(ProceedingJoinPoint pjp) throws Throwable {
        System.out.printf("[Input] %s with argument(s) = %s \n", pjp.getSignature(), Arrays.toString(pjp.getArgs()));
    	long startTime = System.currentTimeMillis();
        Object result = pjp.proceed();
        long endTime = System.currentTimeMillis();
        if(result != null)
        	System.out.printf("[Output] %s with result = %s \n", pjp.getSignature(), result);
        System.out.printf("[Processing Time] %s : %s ms.\n", pjp.getSignature(), (endTime-startTime));
        return result;
    }
}
